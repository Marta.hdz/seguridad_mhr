import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: "AIzaSyBwhaNqRF1Y9qTdUqpD-3dFhbiocWmmpwc",
  authDomain: "seguridad-mhr.firebaseapp.com",
  databaseURL: "https://seguridad-mhr.firebaseio.com",
  projectId: "seguridad-mhr",
  storageBucket: "seguridad-mhr.appspot.com",
  messagingSenderId: "1070432701080",
  appId: "1:1070432701080:web:24402520c0ca00cc77deed"
}

const app = firebase
  .initializeApp(firebaseConfig)

const db = app.firestore()
const storage = app.storage()

export {db, storage}
