import Vue from 'vue'
import Vuex from 'vuex'
import {db} from '../db/db'
import { vuexfireMutations, firestoreAction } from 'vuexfire'
import {uploadFunction} from'../db/uploadImage'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: 'Tester',
    password: '123456',
    logged: false,
    employeeList: [],
    activeCard: '',
  },
  mutations: {
    ...vuexfireMutations,
    login(state, value) {
      state.logged = value
    },
    setActiveCard(state, employeeId) {
      state.activeCard = employeeId
    },
  },
  actions: {
    bindEmployeeList: firestoreAction(({ bindFirestoreRef }) => {
      return bindFirestoreRef('employeeList', db.collection('employeeList'))
    }),
    login({commit}, {username, password, router}) {
      if (username === this.state.username && password === this.state.password) {
        commit('login', true)
        router.push({name: 'Home'})
      }
    },
    logout({commit}, router) {
      commit('login', false)
      router.push({name: 'Login'})
    },
    addEmployee(context, employee) {
          db.collection('employeeList')
            .add(employee)
    },
    editEmployee({dispatch}, updatedEmployee) {
      dispatch('setActiveCard', '')

      db.collection('employeeList')
        .doc(updatedEmployee.id)
        .update(updatedEmployee)
    },

    setActiveCard({commit}, employeeId) {
      commit('setActiveCard', employeeId)
    },
  },
  getters: {
    getLoginState: state => state.logged,
    getEmployeeWithAccessList: state => state.employeeList.filter(employee => employee.access),
    getEmployeeWithoutAccessList: state => state.employeeList.filter(employee => !employee.access),
    getActiveCard: state => {
      if(state.activeCard === '') {return ''}
      return state.employeeList.filter(employee => employee.id === state.activeCard).pop()
    },
  }
})
