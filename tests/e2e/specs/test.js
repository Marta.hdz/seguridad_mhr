// https://docs.cypress.io/api/introduction/api.html

describe('The app', () => {
  it('could add employees without access', () => {
    cy.visit('/')

    cy.get('input[id="username"]')
      .type('Tester')

    cy.get('input[id="password"]')
      .type('123456')

    cy.get('button')
      .click()

      cy.get('input[id="form-name"]')
        .type('Maggie')

      cy.get('input[id="form-surname"]')
        .type('Simpson')

      cy.get('button[id="add-employee"]')
        .click()

      cy.get('#without-access')
        .contains('p', 'Maggie')
  })
})

describe('The app', () => {
  it('The app could add employees with access', () => {
    cy.visit('/')

    cy.get('input[id="username"]')
      .type('Tester')

    cy.get('input[id="password"]')
      .type('123456')

    cy.get('button')
      .click()

    cy.get('input[id="form-name"]')
      .type('Bart')

    cy.get('input[id="form-surname"]')
      .type('Simpson')

    cy.get('[type="checkbox"]')
      .check({force:true})

    cy.get('button[id="add-employee"]')
      .click()

    cy.get('#with-access')
      .contains('p', 'Bart')
  })
})

describe('The app', () => {
  it('The app could modify an employee', () => {
    cy.visit('/')

    cy.get('input[id="username"]')
      .type('Tester')

    cy.get('input[id="password"]')
      .type('123456')

    cy.get('button')
      .click()

    cy.get('input[id="form-name"]')
      .type('Bart')

    cy.get('input[id="form-surname"]')
      .type('Simpson')

    cy.get('[type="checkbox"]')
      .check({force:true})

    cy.get('button[id="add-employee"]')
      .click()

    cy.get('#with-access')
      .contains('p', 'Bart')
      .click()

    cy.get('input[id="form-name"]')
      .type('Test')

      cy.get('button[id="add-employee"]')
        .click()

      cy.get('#with-access')
        .contains('p', 'Test')
        .click()

  })
})
